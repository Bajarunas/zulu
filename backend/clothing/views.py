from django.shortcuts import render
from rest_framework import viewsets
from .models import Clothing, Gender, Type
from .serializers import ClothingSerializer, GenderSerializer, TypeSerializer
from django_filters.rest_framework import DjangoFilterBackend

# Create your views here.
class ClothingViewSet(viewsets.ModelViewSet):
    queryset = Clothing.objects.all()
    serializer_class = ClothingSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['gender']


class GenderViewSet(viewsets.ModelViewSet):
    queryset = Gender.objects.all()
    serializer_class = GenderSerializer


class TypeViewSet(viewsets.ModelViewSet):
    queryset = Type.objects.all()
    serializer_class = TypeSerializer