from django.db import models

# Create your models here.


class Type(models.Model):
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title


class Size(models.Model):
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title


class Gender(models.Model):
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title


class Color(models.Model):
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title


class Designer(models.Model):
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title


class Clothing(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    gender = models.ForeignKey(Gender, related_name='gender_related', on_delete=models.CASCADE)
    type = models.ForeignKey(Type, related_name='type_related', on_delete=models.CASCADE)
    color = models.ForeignKey(Color, related_name='color_related', on_delete=models.CASCADE)
    size = models.ForeignKey(Size, related_name='size_related', on_delete=models.CASCADE)
    designer = models.ForeignKey(Designer, related_name='designer_related', on_delete=models.CASCADE)
    cost = models.FloatField()
    review = models.FloatField()
    image = models.ImageField(upload_to='clothes/', default='default.jpg')

    def __str__(self):
        return self.title