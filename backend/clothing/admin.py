from django.contrib import admin
from .models import Type, Size, Gender, Color, Designer, Clothing

myModels = [Type, Size, Gender, Color, Designer, Clothing]

admin.site.register(myModels)