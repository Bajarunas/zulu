from django.urls import path, include
from .models import Clothing, Gender, Type
from rest_framework import serializers

# Serializers define the API representation.
class ClothingSerializer(serializers.ModelSerializer):
    gender = serializers.ReadOnlyField(source='gender.title')
    type = serializers.ReadOnlyField(source='type.title')
    color = serializers.ReadOnlyField(source='color.title')
    size = serializers.ReadOnlyField(source='size.title')
    designer = serializers.ReadOnlyField(source='designer.title')

    class Meta:
        model = Clothing
        fields = ['id', 'title', 'description', 'gender', 'type', 'color', 'size', 'designer', 'cost', 'review', 'image']


class GenderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Gender
        fields = ['id', 'title']



class TypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Type
        fields = ['id', 'title']