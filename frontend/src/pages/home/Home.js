// Chakra UI
import {
    Container,
    Center,
    SimpleGrid
} from '@chakra-ui/react';

// Components
import ImageBox from './ImageBox';


const Home = () => {
    return (
        <Container maxW='container.xl' mt={200}>
            <SimpleGrid minChildWidth={350} spacing={150}>
                <Center>
                    <ImageBox gender='Men' />
                </Center>
                <Center>
                    <ImageBox gender='Women' />
                </Center>
            </SimpleGrid>
        </Container>
    )
}

export default Home
