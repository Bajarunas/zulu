import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { Box, Image, Text } from '@chakra-ui/react';

// Images & Css
import men from '../../assets/men.jpg';
import woman from '../../assets/woman.jpg';

const ImageBox = ({ gender }) => {
    const imageBox = {
        link: gender === 'Men' ? '/clothing/men' : '/clothing/women',
        image: gender === 'Men' ? men : woman,
        title: gender,
        description: gender === 'Men' ? 'Israel Adesanya' : 'Park Hye Jin'
    }


    return (
        <Link to={imageBox.link}>
            <Box maxW='sm' borderWidth={1} borderRadius='lg'>
                <Image src={imageBox.image} alt={gender} />
                <Box p={6} textAlign='center'>
                    <Text fontSize='2xl' color='black'>{imageBox.title}</Text>
                    <Box
                        color='gray.500'
                        letterSpacing='wide'
                        fontSize='xs'
                        textTransform='uppercase'
                        textAlign='center'
                        mt={2}
                        ml={2}
                    >
                        {imageBox.description}
                    </Box>
                </Box>
            </Box>
        </Link>
    );
};

ImageBox.propTypes = {
    gender: PropTypes.string.isRequired
};

export default ImageBox;