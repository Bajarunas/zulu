import React from 'react';
import PropTypes from 'prop-types';

import { Box, Image, VStack, AspectRatio } from '@chakra-ui/react'
import { StarIcon } from '@chakra-ui/icons'

const ClothingCard = ({ title, cost, image }) => {
    const boxData = {
        title: title,
        cost: cost,
        image: image
    }


    return (
        <Box maxW='sm' borderWidth='1px' borderRadius='lg' overflow='hidden'>
            <AspectRatio maxW='400px' ratio={0.8 / 1}>
                <Image src={boxData.image} alt={boxData.title} />
            </AspectRatio>
            <Box p='6'>
                <VStack >
                    <Box
                        mt='1'
                        fontWeight='semibold'
                        as='h4'
                        lineHeight='tight'
                        isTruncated
                    >
                        {boxData.title}
                    </Box>
                </VStack>
                <Box mt={25}>
                    {boxData.cost}
                    <Box as='span' color='gray.600' fontSize='sm'>
                        / eur
                    </Box>
                </Box>
                <Box>
                    {Array(5)
                        .fill('')
                        .map((_, i) => (
                            <StarIcon
                                key={i}
                                color={i < 4 ? 'teal.500' : 'gray.300'}
                            />
                        ))}
                    <Box as='span' ml='2' color='gray.600' fontSize='sm'>
                        {Math.floor(Math.random() * (100 - 1 + 1) + 1)} reviews
                    </Box>
                </Box>
            </Box>
        </Box >





    );
};

ClothingCard.propTypes = {
    title: PropTypes.string.isRequired,
    cost: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired
};

export default ClothingCard;