import React, { useState, useEffect, useCallback } from 'react';
import DataService from '../../dataservices/DataServices';
import { useMainContext } from "../../context/MainContext";
import { useHistory } from "react-router-dom";
import { Container, Center, FormLabel, Select, Text, SimpleGrid, FormControl, HStack, VStack, Box } from "@chakra-ui/react";

import ClothingCard from './ClothingCard';

const DataServices = new DataService();
export default function Clothing() {
  const [allClothes, setAllClothes] = useState([]);
  const [filteredClothes, setFilteredClothes] = useState([]);

  const [filteredType, setFilteredType] = useState("None");
  const [filteredCost, setFilteredCost] = useState("None");


  const value = useMainContext();
  const history = useHistory();
  const clothingType = window.location.pathname.split("/").pop();


  const getGenderId = useCallback((genderType) => {
    let genderId;
    value.genders.forEach((gender) => {
      if (gender.title === genderType) {
        genderId = gender.id;
      }
    });

    return genderId;
  }, [value.genders]);


  useEffect(() => {
    if (value.genders.length !== 0) {
      let genderId;

      if (clothingType === "men") {
        genderId = getGenderId("Male")
      } else if (clothingType === "women") {
        genderId = getGenderId("Women")
      }

      if (genderId !== undefined) {
        DataServices.getClothingArray(genderId).then((clothes) => {
          setAllClothes(clothes);
          setFilteredClothes(clothes);

          // Reset filters if going through men -> women.
          setFilteredType("None");
          setFilteredCost("None");

        });
      } else {
        history.push("/");
      }
    }
  }, [clothingType, value.genders, history, getGenderId])


  const filterChange = (filterType, value) => {
    let clothes = allClothes;

    if (value === "") {
      value = "None";
    }

    if (filterType === "type" || filteredType !== "None") {
      let typeValue;
      if (filterType === "type") {
        typeValue = value;
      } else {
        typeValue = filteredType;
      }

      if (typeValue !== "None") {
        let filteredClothes = [];
        clothes.forEach((clothing) => {
          if (clothing.type === typeValue) {
            filteredClothes.push(clothing);
          }
        });
        clothes = filteredClothes;
      }
      setFilteredType(typeValue);
    }

    if (filterType === "cost" || filteredCost !== "None") {
      let costValue;
      if (filterType === "cost") {
        costValue = value;
      } else {
        costValue = filteredCost;
      }

      if (costValue !== "None") {
        let costFilteredClothes = JSON.parse(JSON.stringify(clothes));
        if (costValue === "None") {
          costFilteredClothes.sort(function (a, b) {
            return a.id - b.id;
          });

        } else if (costValue === "Cheap") {
          costFilteredClothes.sort(function (a, b) {
            return a.cost - b.cost;
          });
        } else if (costValue === "Expensive") {
          costFilteredClothes.sort(function (a, b) {
            return b.cost - a.cost;
          });
        }
        clothes = costFilteredClothes;
      }
      setFilteredCost(costValue);
    }
    setFilteredClothes(clothes);
  }

  return (
    <Container maxW='container.xl' mt={25}>
      <HStack>
        <VStack>
          <Box w={150}>
            <FormLabel htmlFor='type'>Type</FormLabel>
            <Select placeholder='None' onChange={(e) => filterChange("type", e.target.value)} value={filteredType}>
              {value.clothingTypes.length !== 0 ? value.clothingTypes.map((type) => {
                return <option value={type.title} key={type.title}>{type.title}</option>
              }) : null}
            </Select>
          </Box>
        </VStack>
        <VStack>
          <Box w={150}>
            <FormLabel htmlFor='cost'>Cost</FormLabel>
            <Select placeholder='None' onChange={(e) => filterChange("cost", e.target.value)} value={filteredCost}>
              <option value="Cheap" key="Cheap">Cheap</option>
              <option value="Expensive" key="Expensive">Expensive</option>
            </Select>
          </Box>
        </VStack>
      </HStack>
      <SimpleGrid columns={3} spacing={10} minChildWidth={300} mt={25}>
        {filteredClothes.length !== 0
          ? filteredClothes.map((clothing) => {
            return (
              <ClothingCard title={clothing.title} cost={clothing.cost} image={clothing.image} key={clothing.title} />
            )
          })
          : filteredType !== "None" || filteredCost !== "None" ? <Text fontSize="6xl">Sorry, no clothes found with this filter!</Text> : null}
      </SimpleGrid>
    </Container>
  );
}

