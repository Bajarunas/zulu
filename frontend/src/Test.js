import { SimpleGrid, Box, HStack, FormLabel, Select } from '@chakra-ui/react'

const Test = () => {
  return (
    <>
      <HStack spacing='24px' w="500px">
        <Box w='40px' h='40px' bg='yellow.200'>
          <FormLabel htmlFor='type'>Type</FormLabel>
          <Select placeholder='None'>

            <option>123</option>
            <option>123</option>


          </Select>
        </Box>
        <Box w='40px' h='40px' bg='tomato'>
          2
        </Box>
        <Box w='40px' h='40px' bg='pink.100'>
          3
        </Box>
      </HStack>
    </>
  );
};



export default Test;