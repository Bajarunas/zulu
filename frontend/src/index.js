import React from 'react';
import ReactDOM from 'react-dom';
import './global/global.css';
import App from './App';
import axios from 'axios';
import { ChakraProvider } from "@chakra-ui/react";

axios.defaults.baseURL = process.env.REACT_APP_BE_URL;

ReactDOM.render(
  <React.StrictMode>
    <ChakraProvider>
      <App />
    </ChakraProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

