import axios from "axios";

class DataServices {
  async testDataService() {
    const { data } = await axios
      .get(`https://searchconsole.googleapis.com/$discovery/rest?version=v1`)
      .catch((error) => {
        return error.response;
      });
    return data;
  }

  async getClothingArray(genderId) {
    const { data } = await axios
      .get(`/clothing/`, { params: { gender: genderId } })
      .catch((error) => {
        return error.response;
      });
    return data;
  }

  async getGender() {
    const { data } = await axios
      .get(`/gender/`)
      .catch((error) => {
        return error.response;
      });
    return data;
  }

  async getClothingType() {
    const { data } = await axios
      .get(`/type/`)
      .catch((error) => {
        return error.response;
      });
    return data;
  }

}
export default DataServices;
