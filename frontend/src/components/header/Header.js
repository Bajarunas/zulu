import React from "react";
import { useMainContext } from "../../context/MainContext";
import { Link } from "react-router-dom";

// Material UI
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";

// Ant Design
import { HomeOutlined } from "@ant-design/icons";



const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  spacing: 8,
}));

export default function ButtonAppBar() {
  const classes = useStyles();
  const value = useMainContext();

  return (
    <div className={classes.root}>
      <AppBar position="static" style={{ backgroundColor: "black" }}>
        <Toolbar>
          <Link to="/">
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="menu"
            >
              <HomeOutlined />
            </IconButton>
          </Link>

          <Typography variant="h6" className={classes.title}>
            Zulu
          </Typography>

          <Link to="/" style={value.noDecorationStyle}>
            <Button color="inherit">Home</Button>
          </Link>
          <Link to="/clothing/men" style={value.noDecorationStyle}>
            <Button color="inherit">Men</Button>
          </Link>
          <Link to="/clothing/women" style={value.noDecorationStyle}>
            <Button color="inherit">Women</Button>
          </Link>
          <Link to="/test_component" style={value.noDecorationStyle}>
            <Button color="inherit">Test</Button>
          </Link>
        </Toolbar>
      </AppBar>
    </div>
  );
}

