import { createContext, useContext, useEffect, useState } from 'react';
import DataServices from "../dataservices/DataServices"

const MainContext = createContext();
export const useMainContext = () => useContext(MainContext)

const dataservices = new DataServices();
const noDecorationStyle = { textDecoration: 'none', color: 'white' };
function MainContextProvider(props) {
    const [genders, setGenders] = useState([]);
    const [clothingTypes, setClothingTypes] = useState([]);

    useEffect(() => {
        dataservices.getGender().then((genders) => {
            if (genders.length !== 0) {
                setGenders(genders)
            }
        });

        dataservices.getClothingType().then((clothingTypes) => {
            if (clothingTypes.length !== 0) {
                setClothingTypes(clothingTypes);
            }
        })
    }, [])

    return (
        <MainContext.Provider value={{
            //  State
            noDecorationStyle,
            genders,
            clothingTypes

            //  Functions
        }}>
            {props.children}
        </MainContext.Provider>
    )
}

export default MainContextProvider;