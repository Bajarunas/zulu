import { BrowserRouter as Router, Route } from 'react-router-dom';

// Pages
import Clothing from './pages/clothing/Clothing';
import Home from './pages/home/Home';
import Test from './Test';

// Components
import Header from './components/header/Header';

// Context
import MainContextProvider from "./context/MainContext";

function App() {


  return (
    <MainContextProvider>
      <Router>
        <Header title='App Header' />
        <Route exact path='/' component={Home} />
        <Route exact path='/clothing/:type' component={Clothing} />
        <Route exact path='/test_component' component={Test} />
      </Router>
    </MainContextProvider>
  );
}

export default App;